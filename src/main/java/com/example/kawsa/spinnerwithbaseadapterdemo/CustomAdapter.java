package com.example.kawsa.spinnerwithbaseadapterdemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by kawsa on 04-Mar-18.
 */

public class CustomAdapter extends BaseAdapter {
    Context context;
    int[] flags;
    String[] countryName;

    public CustomAdapter(Context context, int[] flags, String[] countryName) {
        this.context = context;
        this.flags = flags;
        this.countryName = countryName;
    }

    @Override
    public int getCount() {
        return countryName.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view =  inflater.inflate(R.layout.sample_view,null,false);
        }
        ImageView imageView = view.findViewById(R.id.imageViewId);
        TextView textView = view.findViewById(R.id.ctextId);
        imageView.setImageResource(flags[i]);
        textView.setText(countryName[i]);
        return view;
    }
}
