package com.example.kawsa.spinnerwithbaseadapterdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Spinner spinner;
    private Button button;
    private TextView textView;
    String[] countryName;
    int[] flags = {
            R.drawable.bangladesh,
            R.drawable.pakistan,
            R.drawable.india,
            R.drawable.nepal,
            R.drawable.afghanistan,
            R.drawable.china
    };
    private boolean isSelected = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        countryName = getResources().getStringArray(R.array.countyname);

        spinner = findViewById(R.id.spinnerId);
        button = findViewById(R.id.printId);
        textView = findViewById(R.id.shoTextId);
        CustomAdapter adapter = new CustomAdapter(this,flags,countryName);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (isSelected == true){
                    isSelected =false;
                }else {
                    Toast.makeText(MainActivity.this,countryName[i]+" is selected",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}
